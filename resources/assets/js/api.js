/**
 *
 * @type {{
 *  apiUrl: string,
 *  amount: number,
 *  bookUrl: string,
 *  getAllBook: $api.getAllBook,
 *  getActiveBook: $api.getActiveBook,
 *  bookWhereAmount: $api.bookWhereAmount,
 *  download: $api.download,
 *  setAmount: $api.setAmount
 *  }}
 */
var $api = {

    /** Api URL **/
    apiUrl: 'http://127.0.0.1:8080/api/v1',

    /** **/
    amount: 10,

    bookUrl: '/book',

    getAllBook: function() {
        $api.apiUrl = $api.apiUrl + $api.bookUrl + '/all';
    },

    getActiveBook: function() {
        $api.apiUrl = $api.apiUrl + $api.bookUrl + '/active';
    },

    bookWhereAmount: function($amount) {
        $api.apiUrl = $api.apiUrl + $api.bookUrl + '/amount/' + $amount;
    },

    download: function(funCallback) {
        $.get($api.apiUrl, function(getCallback) {
            funCallback(getCallback);
        });
    },

    setAmount: function($amount) {
        $api.amount = $amount;
    }

};

