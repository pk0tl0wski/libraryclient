@extends('layout.layout')

@section('sub_title')
    <h2 class="sub-header">Więcej niż dziesięć na stanie</h2>
@endsection

@section('content')
    <table class="table table-striped book-table">
        <thead>
        <tr>
            <th>#</th>
            <th>Nazwa</th>
            <th>Ilosc</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".book-table tbody").html('');
            $api.getMoreThanTen(function(callback){
                $.each(callback, function(k,v){
                    $(".book-table tbody").append('<tr>\n\
                        <td>'+ v.id + '</td>\n\
                        <td>'+ v.title +'</td>\n\
                        <td>'+ v.amount +'</td>\n\
                        </tr>');
                });
            });
        })
    </script>
@endsection