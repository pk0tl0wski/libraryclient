@extends('layout.layout')

@section('content')
    <table class="table table-striped not-exist-book">
        <thead>
        <tr>
            <th>#</th>
            <th>Nazwa</th>
            <th>Ilosc</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".not-exist-book tbody").html('');
            $api.getNotExistBook(function(callback){
                $.each(callback, function(k,v){
                    $(".not-exist-book tbody").append('<tr>\n\
                        <td>'+ v.id + '</td>\n\
                        <td>'+ v.title +'</td>\n\
                        <td>'+ v.amount +'</td>\n\
                        </tr>');
                });
            });
        })
    </script>
@endsection