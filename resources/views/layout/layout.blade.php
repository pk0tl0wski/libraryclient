
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="api" content="{{ config('app.api_url') }}">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Dashboard Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ url('css/vendor.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ url('css/dashboard.css') }}" rel="stylesheet">
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Książki</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse"></div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li><a>Książki</a></li>
                <li {{ Request::is('book/exist')? "class=active":'' }}><a href="{{ route('book_exist') }}">Są na stanie</a></li>
                <li {{ Request::is('book/notexist')? "class=active":'' }}><a href="{{ route('book_notexist') }}">Brak na stanie</a></li>
                <li {{ Request::is('book/ten')? "class=active":'' }}><a href="{{ route('book_ten') }}">Ilość powyżej 10</a></li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            @yield('sub_title')
            <div class="table-responsive">
                @yield('content')
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<script src="{{ url('js/vendor.js') }}" type="text/javascript"></script>
<script src="{{ url('js/api.js') }}" type="text/javascript"></script>
@yield('scripts')
</body>
</html>
