<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BookController@exist');
Route::get('/book', 'BookController@exist');

Route::get('/book/exist', 'BookController@exist')->name('book_exist');
Route::get('/book/notexist', 'BookController@notexist')->name('book_notexist');
Route::get('/book/ten', 'BookController@moreTen')->name('book_ten');
