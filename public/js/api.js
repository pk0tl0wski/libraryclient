/**
 *
 * @type {{
 *  apiUrl: string,
 *  amount: number,
 *  bookUrl: string,
 *  getAllBook: $api.getAllBook,
 *  getActiveBook: $api.getActiveBook,
 *  bookWhereAmount: $api.bookWhereAmount,
 *  download: $api.download,
 *  setAmount: $api.setAmount
 *  }}
 */
var $api = {

    /** Api URL **/
    apiUrl: $('meta[name="api"]').attr('content'),

    /** **/
    amount: 10,

    bookUrl: '/book',

    getExistBook: function(callback) {
        var $apiUrl = $api.apiUrl + $api.bookUrl + '/exists';

        $.get($apiUrl, function(getCallback) {
            callback(getCallback.data);
        });
    },

    getNotExistBook: function(callback) {
        var $apiUrl = $api.apiUrl + $api.bookUrl + '/notexists';

        $.get($apiUrl, function(getCallback) {
            callback(getCallback.data);
        });
    },

    getMoreThanTen: function(callback) {
        var $apiUrl = $api.apiUrl + $api.bookUrl + '/more/ten' ;

        $.get($apiUrl, function(getCallback) {
            callback(getCallback.data);
        });
    },

    setAmount: function($amount) {
        $api.amount = $amount;
    }

};